import { defineStore } from "pinia";

export const useCitiesStore = defineStore({
    id: 'cities',
    state: () => ({
        cities: [
            { id: -290692, name: 'Cairo' },
            { id: -290029, name: 'Sharm El Sheikh' },
            { id: -302053, name: 'Hurghada' },
          ],
    }),
  });
export const useSearchStore = defineStore({
    id: 'search',
    state: () => ({
      hotels: [],
      dest_id: '',
      searchCity: '',
      cities: [],
    }),
    actions: {
        setHotels(hotels) {
          this.hotels = hotels;
        },
        setDestId(dest_id) {
          this.dest_id = dest_id;
        },
        setSearchCity(searchCity) {
          this.searchCity = searchCity;
        },
        setCities(cities) {
          this.cities = cities;
        },
      },
})


