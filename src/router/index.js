import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '../views/Login.vue'
import Reservation from '../views/Reservation.vue'
import HotelDetails from '../views/HotelDetails.vue'
import ConfirmBooking from '../views/ConfirmBooking.vue'
import ListOfBookings from '../views/ListOfBookings.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Reservation',
      name: 'Reservation',
      component: Reservation,
      meta: {
        requiresAuth: true
      }
    },
    {
      path:'/HotelDetails',
      name:'HotelDetails',
      component:HotelDetails,
      meta: {
        requiresAuth: true
      }
    },
    {
      path:'/ConfirmBooking',
      name:'ConfirmBooking',
      component:ConfirmBooking,
      meta: {
        requiresAuth: true
      }
    },
    {
      path:'/ListOfBookings',
      name:'ListOfBookings',
      component:ListOfBookings,
      meta: {
        requiresAuth: true
      }
    },
  ]
})
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const token = localStorage.getItem('token');
    if (token) {
      next();
    } else {
      next('/login');
    }
  } else {
    next();
  }
});
export default router
