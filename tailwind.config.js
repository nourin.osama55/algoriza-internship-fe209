/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html","./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      container:{
        padding:"6rem",
        center:true,
        margin:0,
        overflow:"hidden",
      },
      colors:{
        "blue-1":"#2F80ED",
        "gray-1":"#333333",
        "white-1":"#FFFFFF",
        "warning":"#FCEFCA",
        "gray-2":"#4F4F4F",
        "gray-3":"#EBEBEB",
        "gray-4":"#F2F2F2",
        "gray-5":"#666666", 
        "top-blue":"#2969BF",
        "bottom-blue":"#144E9D",
        "orange-1":"#F2994A",
        "yellow-1":"#F2C94C",
        "red-1":"#EB5757",
        "mint-green":"#85E0AB"
      },
      fontFamily:"'Poppins', sans-serif",
    },
  },
  plugins: [],
}

